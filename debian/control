Source: python-daiquiri
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Chris Lamb <lamby@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-setuptools,
 python3-setuptools-scm,
 python3-testtools,
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/python-team/packages/python-daiquiri.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-daiquiri
Homepage: https://github.com/jd/daiquiri

Package: python3-daiquiri
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Python library to easily setup basic logging functionality
 The daiquiri library provides an easy way to configure logging. It also
 provides some custom formatters and handlers.
 .
 Its promise is to setup a complete standard Python logging system with just
 one function call. Nothing more, nothing less. The interesting features are:
 .
  * Logs to stderr by default.
  * Uses colors if logging to a terminal.
  * File-based logging.
  * Uses program name as the name of the logging file (so providing just a
    directory for logging will work).
  * syslog & journald support.
  * JSON output.
  * Arbitrary key/value context information providing.
  * Capture the warnings emitted by the warnings module.
  * Native logging of any exception.
 .
 This is the Python 3 version of the package.
